secSizeDist.py computes the size distribution of firms in the US by sector. It also aggregates the distribution to major NAICS level and major SIC level.


The input data is by NAICS codes. 
Below is a description of the different functions in the program.

Input File

(a) sizeDist_sector.csv (from Small Business Administration on size distribution) 
(b) conversion_table.csv (conversion of NAICS2002 to SIC codes, from Census)
(c) additionalConverstion.txt (some NAICS SIC codes are converted manually using the NAICS website, these are the codes  not present in the conversion table from Census)



The Functions

naicsCodes()
Use the file on size distribution by sector to write a txt file with only the NAICS codes for which distribution data is available. Note that this includes all NAICS codes in the input file, subdivisions to whichever level available. For some sectors the subdivisions are fewer than others.
Output: naics.txt

conversionCodes()
For every NAICS code with distribution data, match a corresponding SIC code.
Output: conversion.txt, first element is NAICS, second is SIC

sizeNaics() 
This function does much of the work. It writes a bunch of files in the folder Interim files, which records sizes of different variables by sector. For instance the file revS1.txt has the sizes of firms with revenues of less than 1 million for different sectors, and revS10.txt has revenues of less than 10 million.


minimalCodes()
Reduces the naics codes to the lowest level without double counting, whenever there are lower level codes available, the upper level codes are deleted

dataMinimalCodes()
Subsets all the data takes by sizeNaics to those pertaining to the minimal codes


SIC()
Convert the sectors distribution by NAICS to distribution by SIC codes

SICtwoDigit()
Aggregate sectors distribution by SIC codes to two digit level

majorIndustry()
Aggregate SIC two digit distribution to SIC major industry level

majNAICS()
Aggregate NAICS distribution to NAICS major industry (with more aggregation than usual NAICS major industry)

smallTotalNaics()
Write the receipts of small firms and total receipts by NAICS codes. Small is firms which makes less than hundred thousand, one million, and ten million.

all()
Runs all the above functions to process data




