from __future__ import division
import pandas as pd
import numpy as np
import ast
import csv
import cPickle
import matplotlib.pyplot as plt
import ast
import collections
import math
import os


def write(dataDict):
    for name in dataDict:
        data = dataDict[name]
        fileName = name + '.txt'
        with open(fileName, 'w') as file:
            for k in data:
                value = data[k]
                pair = [k, value]
                pair = ','.join(map(str, pair))
                file.write(pair + '\n')

def inDirect():
    child_directory = 'interimFiles'
    if not os.path.exists(child_directory):
        os.makedirs(child_directory)
    os.chdir(child_directory)

def outDirect():
    os.chdir('..')

def IntKnown(v):
    a = 0
    if isinstance(v, int):
        a += int(v)
    elif isinstance(v, float):
        if not math.isnan(v):
            a += int(v)
    else:
        if v.isdigit():
            a += int(v)
    return a

def naicsCodes():
    # use the sectoral distribution of sizes to write a txt file with only the NAICS codes for which distribution is available
    data = pd.read_csv('sizeDist_sector.csv')
    data = pd.DataFrame(data)
    d = data.iloc[:]['NAICS']  # load all the NAICS codes
    d = d.dropna()  # remove the empty lines entries
    d = list(d)  # make a list
    d = d[1:]  # drop the first element which is "-"
    inDirect()
    with open('naics.txt', 'w') as file:
        # write the naics codes into a txt file
        for code in d:
            file.write(code + '\n')
    outDirect()

def conversionCodes():
    #For every NAICS code with distribution data, match a corresponding SIC code.
    conversion_dictionary = {}
    data = pd.read_csv("conversionTable.csv")
    data = pd.DataFrame(data)
    rows = data.shape[0]
    # make a dictionary out of the converstionTable
    for r in xrange(rows):
        naics = data.iloc[r]['NAICS']
        sic = data.iloc[r]['SIC']
        conversion_dictionary[str(naics)] = str(sic)

    for n in conversion_dictionary:
        sic = conversion_dictionary[n]
        if len(sic) < 4:  # if the SIC code does not have a zero at the beginning add 0,
            # so that code is 08 and not 8, or 0872 and not 872
            sic = '0' + sic
            conversion_dictionary[n] = sic

    inDirect()
    with open('naics.txt', 'r') as file, open('conversion.txt', 'w') as writeFile:
        inds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        inds = [str(i) for i in inds]
        absent = []

        # there are six naics for which sic is not available in the Table, these were manually taken from the NAICS website
        # the ones manually taken from the naics website are in 'additionalConverstion.txt', these are added to the overall fiel
        num_lines = sum(1 for line in open('additionalConverstion.txt'))
        count = 0
        with open('additionalConverstion.txt', 'r') as add:
            for line in add:
                count += 1
                if count < num_lines:
                    writeFile.write(line)
                else:
                    writeFile.write(line + '\n')

        # the code below resolves the following problem:
        # the conversion table has 6 digit NAICS codes to SIC code conversion
        # it does not 3,4 or 5 digit NAICS codes to SIC code conversion
        # so to a 5 digit code we add a digit, find the corresponding SIC code,
        # and then reduce the SIC code to the first 2 digits

        for line in file:
            naics = line[:-1]
            if len(naics) == 6:
                sic = conversion_dictionary[naics]
                pair = [naics, sic]
                pair = ','.join(map(str, pair))
                writeFile.write(pair + '\n')
            elif len(naics) == 5:
                newNaics = [naics + j for j in inds]
                for n in newNaics:
                    codes = []
                    if n in conversion_dictionary:
                        sic = conversion_dictionary[n]
                        codes.append(sic)
                    if len(codes) > 0:
                        sic = codes[0][:-2]  # reduce the SIC code to first two digits
                        # because arbitrary digits were added to the corresponding naics codes
                        pair = [naics, sic]
                        pair = ','.join(map(str, pair))
                        writeFile.write(pair + '\n')
            elif len(naics) == 4:
                newNaics = [naics + '1' + j for j in inds]
                for n in newNaics:
                    codes = []
                    if n in conversion_dictionary:
                        sic = conversion_dictionary[n]
                        codes.append(sic)
                    if len(codes) > 0:
                        sic = codes[0][:-2]  # reduce the SIC code to first two digits
                        # because arbitrary digits were added to the corresponding naics codes
                        pair = [naics, sic]
                        pair = ','.join(map(str, pair))
                        writeFile.write(pair + '\n')
            else:
                absent.append(naics)
    outDirect()

def smallTotalNaics(): # reciepts of small firms and total reciepts for each naics code
    data = pd.read_csv('sizeDist_sector.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0] - 1
    heads = ['Total',
             'Est. Receipt size classes, $<100,000',
             '$100,000-499,000',
             '$500,000-999,000',
             '$1,000,000-2,499,000',
             '$2,500,000-4,999,000',
             '$5,000,000-7,499,000',
             '$7,500,000-9,999,000']
    fileName = 'naicsSizes'
    with open('%s.csv' % fileName, 'wb') as data_csv:
        writer_data = csv.writer(data_csv, delimiter=',')
        column_names = ['naics', 'all', 'small0', 'small1', 'small10']
        writer_data.writerow(column_names)
        for r in xrange(3, rows):
            if data.iloc[r]['Type of Data (pay. and rec. $000)'] == 'firms':
                ind = r
                naics = data.iloc[ind]["NAICS"]
                naics = str(naics)
                i = ind + 4
                temp_val = data.iloc[i]['Total']
                all = IntKnown(temp_val)

                temp_val = data.iloc[i]['Est. Receipt size classes, $<100,000']
                small0 = IntKnown(temp_val)

                small1 = small0 + \
                         IntKnown(data.iloc[i]['$100,000-499,000']) + \
                         IntKnown(data.iloc[i]['$500,000-999,000'])
                small10 = small1 + IntKnown(data.iloc[i]['$1,000,000-2,499,000']) + \
                          IntKnown(data.iloc[i]['$2,500,000-4,999,000']) + \
                          IntKnown(data.iloc[i]['$5,000,000-7,499,000']) + \
                          IntKnown(data.iloc[i]['$7,500,000-9,999,000'])
                writer_data.writerow([naics] + [all, small0, small1, small10])

def sizeNaics():
    # here 1 refers to those who have a revenues of less than 10^6 USD, and 10 to less than 10^7 USD
    # therefore S10 includes S1, ALL refers to all firms

    variables = ['firm', 'rev', 'emp', 'wage']
    # firm means number of firms
    # rev are reciepts
    # emp is simply employment
    # wage is the total wage bill

    parameters = ['All', 'S0', 'S1', 'S10']
    heads = ['Total',
             'Est. Receipt size classes, $<100,000',
             '$100,000-499,000',
             '$500,000-999,000',
             '$1,000,000-2,499,000',
             '$2,500,000-4,999,000',
             '$5,000,000-7,499,000',
             '$7,500,000-9,999,000']
    indexesParameters = {'All': [0], 'S0': [1], 'S1': range(1, 4), 'S10': range(1, 8)}
    indexesVariables = {'firm': 0, 'rev': 4, 'emp': 2, 'wage': 3}

    names = []
    codes = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})

    data = pd.read_csv('sizeDist_sector.csv')
    data = pd.DataFrame(data)
    rows = data.shape[0] - 1

    wageAll = 0
    wageS0 = 0
    wageS1 = 0
    wageS10 = 0
    for r in xrange(6, rows):
        if data.iloc[r]['Type of Data (pay. and rec. $000)'] == 'firms':
            ind = r
            description = data.iloc[ind]["Description"]
            naics = data.iloc[ind]["NAICS"]
            naics = str(naics)
            codes.append(naics)
            for v in variables:
                for p in parameters:
                    name = v + p
                    i = ind + indexesVariables[v]
                    val = 0
                    for j in indexesParameters[p]:
                        h = heads[j]
                        temp_val = data.iloc[i][h]
                        val += IntKnown(temp_val)
                    all[name][naics] = val
                    if v == 'wage':
                        if p == 'All':
                            wageAll += val
                        elif p == 'S0':
                            wageS0 += val
                        elif p == 'S1':
                            wageS1 += val
                        elif p == 'S10':
                            wageS10 += val
    print wageS0/wageAll, "prop wage S0"
    print wageS1 / wageAll, "prop wage S1"
    print wageS10 / wageAll, "prop wage S10"


    with open('naicsCodesDist.txt', 'w') as codesFile: # is there a difference between naics.txt and naicsCodesDist.txt, if not why repeat?
        for c in codes:
            c = str(c)
            codesFile.write(c + '\n')

    inDirect()
    write(dataDict=all)

    outDirect()

def minimalCodes():
    # reduces the naics codes to the lowest level without double counting
    # whenever there are lower level codes available, the upper level codes are deleted

    codes = []
    with open("naicsCodesDist.txt", 'r') as file:
        for line in file:
            c = line[:-1]
            codes.append(c)
    nextDigit = range(0, 10)
    nextDigit = [str(i) for i in nextDigit]

    # remove all codes which are three or fewer digits because they all have lower level codes
    for c in codes:
        if len(c) <= 3:
            codes.remove(c)

    leng = [3, 4, 5, 6]
    remove = []
    for l in leng:
        for c in codes:
            if len(c) <= l:
                newCodes = [c + j for j in nextDigit]
                for nC in newCodes:
                    if nC in codes:
                        remove.append(c)
    remove = set(remove)
    for item in remove:
        codes.remove(item)

    remove = []
    for c in codes:
        newCodes = [c + '1' + j for j in nextDigit]
        for nC in newCodes:
            if nC in codes:
                remove.append(c)
    remove = set(remove)
    for item in remove:
        codes.remove(item)

    remove = []
    for c in codes:
        newCodes = [c + '11' + j for j in nextDigit]
        for nC in newCodes:
            if nC in codes:
                remove.append(c)
    remove = set(remove)
    for item in remove:
        codes.remove(item)

    inDirect()
    with open('minimalCodes.txt', 'w') as file:
        for c in codes:
            file.write(str(c) + '\n')
    outDirect()

def dataMinimalCodes():
    # subset all the written data to only those which pertain to codes in minimal naics codes,
    # i.e. codes at the lowest level of aggregation without repetition.
    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['All', 'S0', 'S1', 'S10']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})
    minimalCodes = []

    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                naics = line[0]
                val = int(line[1])
                all[n][naics] = val
    with open('minimalCodes.txt', 'r') as file:
        for line in file:
            code = line[:-1]
            minimalCodes.append(code)

    allMin = {}
    for n in names:
        nMin = n + 'Min'
        allMin[nMin] = collections.OrderedDict({})

    for c in minimalCodes:
        if '-' not in c:
            for name in names:
                val = all[name][c]
                name += 'Min'
                allMin[name][c] = val

    write(dataDict=allMin)

    outDirect()

def SIC():
    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['AllMin', 'S0Min', 'S1Min', 'S10Min']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})

    allSIC = {}
    for n in names:
        nSIC = n + 'SIC'
        allSIC[nSIC] = collections.OrderedDict({})

    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                naics = line[0]
                val = int(line[1])
                all[n][naics] = val

    conversion = collections.OrderedDict({})
    with open('conversion.txt', 'r') as file:
        for line in file:
            line = line.split(',')
            naics = line[0]
            sic = line[1]
            sic = sic.rstrip('\n')
            conversion[naics] = sic

    SIC_codes = conversion.values()
    for sic in SIC_codes:
        for name in names:
            nSIC = name + 'SIC'
            allSIC[nSIC][sic] = 0

    for name in names:
        data = all[name]
        nameSIC = name + 'SIC'
        for naics in data:
            if naics in conversion:
                sic = conversion[naics]
                value = data[naics]
                allSIC[nameSIC][sic] += int(value)

    write(dataDict=allSIC)
    outDirect()

def SICtwoDigit():
    # aggregate distribution data by SIC codes to SIC two digit level
    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['AllMinSIC', 'S0MinSIC', 'S1MinSIC', 'S10MinSIC']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})
    all2D = {}
    for n in names:
        n2D = n + '2D'
        all2D[n2D] = collections.OrderedDict({})

    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                naics = line[0]
                val = int(line[1])
                all[n][naics] = val

    allDigits = []
    for n in names:
        allDigits += all[n].keys()
    allDigits = list(set(allDigits))
    twoDigits = []
    for digit in allDigits:
        two = digit[0:2]
        twoDigits.append(two)
    twoDigits = list(set(twoDigits))
    for d in twoDigits:
        if len(d) == 1:
            newD = '0' + d
            twoDigits.remove(d)
            twoDigits.append(newD)

    for name in names:
        data = all[name]
        name2D = name + '2D'
        for sic in twoDigits:
            val = 0
            for d in allDigits:
                if d[0:2] == sic:
                    v = data[d]
                    val += v
            all2D[name2D][sic] = val

    write(dataDict=all2D)
    outDirect()

def majorIndustry():
    # aggregate distributions by SIC two digit to SIC major industry classification
    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['AllMinSIC2D', 'S0MinSIC2D', 'S1MinSIC2D', 'S10MinSIC2D']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})
    allMajor = {}
    for n in names:
        nMajor = n + 'Major'
        allMajor[nMajor] = collections.OrderedDict({})

    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                sic = line[0]
                val = int(line[1])
                all[n][sic] = val

    man = range(20, 40)
    man = [str(i) for i in man]
    tpu = range(40, 50)
    tpu = [str(i) for i in tpu]
    ret = range(52, 60)
    ret = [str(i) for i in ret]
    fire = range(60, 68)
    fire.remove(66)
    fire = [str(i) for i in fire]
    ser = range(70, 90)
    ser.remove(71)
    ser.remove(74)
    ser.remove(77)
    ser = [str(i) for i in ser]

    majorIndustry = {'AGR': ['01', '02', '07', '08', '09'],
                     'MIN': ['10', '12', '13', '14'],
                     'CON': ['15', '16', '17'],
                     'MAN': man,
                     'TCU': tpu,
                     'WHO': ['50', '51'],
                     'RET': ret,
                     'FIRE': fire,
                     'SER': ser}
    majorIndustryCodes = {}

    for group in majorIndustry:
        codes = majorIndustry[group]
        for c in codes:
            majorIndustryCodes[c] = group
        for n in allMajor:
            allMajor[n][group] = 0

    for name in names:
        data = all[name]
        for sic in data:
            val = data[sic]
            if sic in majorIndustryCodes:
                group = majorIndustryCodes[sic]
                n = name + 'Major'
                allMajor[n][group] += val

    write(dataDict=allMajor)

    outDirect()

def naicsMAJ():
    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['All', 'S0', 'S1', 'S10']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})

    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                sic = line[0]
                val = int(line[1])
                all[n][sic] = val
    codes = []
    for n in names:
        data = all[n]
        codes += data.keys()
    codes = list(set(codes))
    twoDigit = []
    threeDigit = []
    fourDigit = []
    keepCodes = []
    for c in codes:
        if len(c) == 2:
            twoDigit.append(c)
        elif len(c) == 3:
            threeDigit.append(c)
        elif len(c) == 4:
            fourDigit.append(c)

    for c in twoDigit:
        remove = []
        for code in codes:
            a = code[0:2]
            if a == c:
                remove.append(code)
                keepCodes.append(c)
        remove = set(remove)
        for j in remove:
            codes.remove(j)

    for c in threeDigit:
        remove = []
        for code in codes:
            a = code[0:3]
            if a == c:
                remove.append(code)
                keepCodes.append(c)
        remove = set(remove)
        for j in remove:
            codes.remove(j)

    keepCodes = list(set(keepCodes))
    keepCodesDict = {}
    for c in keepCodes:
        keepCodesDict[c] = c[0:2]
    keeps = list(set(keepCodesDict.values()))
    allMAJ = {}
    for n in names:
        nMAJ = n + 'MAJ'
        allMAJ[nMAJ] = collections.OrderedDict({})
        for k in keeps:
            allMAJ[nMAJ][k] = 0

    for n in names:
        data = all[n]
        nMAJ = n + 'MAJ'
        for k in keepCodes:
            val = data[k]
            ind = keepCodesDict[k]
            allMAJ[nMAJ][ind] += val

    write(allMAJ)
    outDirect()

def majNAICS():
    # aggregate distribution by NAICS codes to NAICS major industry
    """
    INDS = {11: 'AGR', 21: 'MIN', 23: 'CON', 22: 'UTI', 31: 'MAN', 32: 'MAN', 33: 'MAN', 42: 'WHO', 44: 'RET',
            45: 'RET', 48: 'TWH', 49: 'TWH', 51: 'INF', 52: 'FIN', 53: 'RLE', 54: 'PBS', 55: 'PBS', 56: 'PBS',
            61: 'EHS', 62: 'EHS', 71: 'AER', 72: 'AFS', 81: 'SER'}

    INDS = collections.OrderedDict({11: 'AGR', 21: 'MIN', 23: 'CON', 22: 'Others', 31: 'MAN', 32: 'MAN', 33: 'MAN', 42: 'WHO', 44: 'RET',
            45: 'RET', 48: 'Others', 49: 'Others', 51: 'Others', 52: 'Others', 53: 'Others', 54: 'Others', 55: 'Others', 56: 'Others',
            61: 'Others', 62: 'Others', 71: 'SER', 72: 'SER', 81: 'SER'})

    INDS = collections.OrderedDict({11: 'AGR', 71: 'SER', 72: 'SER', 81: 'SER',44: 'RET',
         45: 'RET', 42: 'WHO', 23: 'CON',21: 'MIN', 31: 'MAN', 32: 'MAN', 33: 'MAN', 22: 'Others', 48: 'Others', 49: 'Others', 51: 'Others', 52: 'Others', 53: 'Others', 54: 'Others', 55: 'Others',
         56: 'Others', 61: 'Others', 62: 'Others'})
    """
    AGR = 'Agriculture'
    SER = 'Services'
    CON = 'Construction'
    RET = 'Retail'
    WHO = 'Wholesale'
    MAN = 'Manufacturing'
    MIN = 'Mining'
    UTI = 'Utilities'
    Others = 'Others'

    INDS = collections.OrderedDict({})
    INDS[11] = AGR
    INDS[71] = SER
    INDS[72] = SER
    INDS[81] = SER
    INDS[61] = SER
    INDS[62] = SER
    INDS[23] = CON
    INDS[44] = RET
    INDS[45] = RET
    INDS[42] = WHO
    INDS[31] = MAN
    INDS[32] = MAN
    INDS[33] = MAN
    INDS[21] = MIN
    INDS[22] = UTI
    INDS[48] = Others
    INDS[49] = Others
    INDS[51] = Others
    INDS[52] = Others
    INDS[53] = Others
    INDS[54] = Others
    INDS[55] = Others
    INDS[56] = Others
    INDS[61] = Others
    INDS[62] = Others

    MAJ = [AGR, SER, CON, RET, WHO, MAN, MIN, UTI, Others]
    # MAJ = ['AGR', 'SER', 'RET', 'WHO', 'MAN', 'MIN', 'UTI', 'Others']
    MAJDICT = collections.OrderedDict({})
    for m in MAJ:
        MAJDICT[m] = 0

    variables = ['firm', 'rev', 'emp', 'wage']
    parameters = ['AllMAJ', 'S0MAJ', 'S1MAJ', 'S10MAJ']
    names = []
    for v in variables:
        for p in parameters:
            n = v + p
            names.append(n)
    all = {}
    for n in names:
        all[n] = collections.OrderedDict({})
    inDirect()
    for n in names:
        fileName = n + '.txt'
        with open(fileName, 'r') as file:
            for line in file:
                line = line.split(',')
                sic = line[0]
                val = int(line[1])
                all[n][sic] = val

    allGroup = {}
    for n in names:
        nGroup = n + 'Group'
        allGroup[nGroup] = collections.OrderedDict({})

    for maj in MAJ:
        for name in names:
            nGroup = name + 'Group'
            allGroup[nGroup][maj] = 0

    for n in names:
        data = all[n]
        nGroup = n + 'Group'
        for i in INDS:
            v = data[str(i)]
            maj = INDS[i]
            allGroup[nGroup][maj] += v

    write(dataDict=allGroup)
    outDirect()

def plots(fileNames, title, ylabel, saveName, hist):
    all = collections.OrderedDict({})
    s10 = collections.OrderedDict({})
    s1 = collections.OrderedDict({})
    s0 = collections.OrderedDict({})
    inDirect()
    f0 = fileNames[0]
    f1 = fileNames[1]
    f2 = fileNames[2]
    f3 = fileNames[3]

    with open(f0, 'r') as file:
        for line in file:
            line = line.split(",")
            group = line[0]
            val = line[1]
            val = val.rstrip('\n')
            all[group] = val
    with open(f1, 'r') as file:
        for line in file:
            line = line.split(",")
            group = line[0]
            val = line[1]
            val = val.rstrip('\n')
            s0[group] = val
    with open(f2, 'r') as file:
        for line in file:
            line = line.split(",")
            group = line[0]
            val = line[1]
            val = val.rstrip('\n')
            s1[group] = val
    with open(f3, 'r') as file:
        for line in file:
            line = line.split(",")
            group = line[0]
            val = line[1]
            val = val.rstrip('\n')
            s10[group] = val

    s0Prop = collections.OrderedDict({})
    s1Prop = collections.OrderedDict({})
    s10Prop = collections.OrderedDict({})

    verySmallVal = 0
    smallVal = 0
    allVal = 0

    for i in s1.values():
        verySmallVal += int(i)


    for i in s10.values():
        smallVal += int(i)

    for i in all.values():
        allVal += int(i)

    econVery = verySmallVal/allVal
    econSmallShare = smallVal/allVal
    print econVery, "very small share in economy"
    print econSmallShare, "small share in economy"

    for g in s0:
        if int(s0[g]) > 0 and int(all[g]) >0:
            p = int(s0[g]) / int(all[g])
            s0Prop[g] = p
        else:
            s0Prop[g] = 0

    for g in s1:
        if int(s1[g]) > 0 and int(all[g]) >0:
            p = int(s1[g]) / int(all[g])
            s1Prop[g] = p
        else:
            s1Prop[g] = 0

    for g in s10:
        if int(s10[g]) > 0 and int(all[g]) >0:
            p = int(s10[g]) / int(all[g])
            s10Prop[g] = (p)
        else:
            s10Prop[g] = 0

    names = s1.keys()
    names_IDS = range(0, len(names))
    if hist:
        bins = 25
        color = "cornflowerblue"
        v = s0Prop.values()
        v.sort()
        plt.hist(v, label=r'Receipts < USD $10^5$', bins=bins, color=color, edgecolor='black')
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend()
        plt.savefig("hist0")
        plt.close()

        v = filter(lambda a: a != 0, v)
        v = [math.log(i,10) for i in v]
        v.sort()

        plt.hist(v, label=r'Receipts < USD $10^5$', bins=bins, color=color)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts (log base 10)', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend(loc=2)
        plt.savefig("loghist0")
        plt.close()

        codeDigits = {}
        for code in s1Prop:
            l = len(code)
            if l not in codeDigits:
                codeDigits[l] = 1
            else:
                codeDigits[l]+= 1
        print codeDigits, "number of values in hist by size of codes"
        v = s1Prop.values()
        v.sort()
        print len(v), "number of values in histogram"
        plt.hist(v, label=r'Receipts < USD $10^6$', bins=bins, color=color)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend()
        plt.savefig("hist1")
        plt.close()

        much = 0
        little = 0
        for val in v:
            if val < 0.01:
                little += 1
            elif val > 0.1:
                much += 1
        print little, "little", much, "much"

        v = filter(lambda a: a != 0, v)
        v = [math.log(i, 10) for i in v]
        v.sort()

        plt.hist(v, label=r'Receipts < USD $10^6$', bins=bins, color=color)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts (log base 10)', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend(loc=2)
        plt.savefig("loghist1")
        plt.close()

        v = s10Prop.values()
        v.sort()
        plt.hist(v, label=r'Receipts < USD $10^7$', bins=bins, color=color)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend()
        plt.savefig("hist10")
        plt.close()

        v = filter(lambda a: a != 0, v)
        v = [math.log(i, 10) for i in v]
        v.sort()
        plt.hist(v, label=r'Receipts < USD $10^7$', bins=bins, color=color)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.xlabel('Share of small firms in receipts (log base 10)', fontsize=14)
        plt.ylabel('Number of sectors', fontsize=14)
        #plt.legend(loc=2)
        plt.savefig("loghist10")
        plt.close()

    else:
        fig, ax = plt.subplots()
        fig.subplots_adjust(bottom=0.2)
        ax.scatter(names_IDS, s1Prop.values(), label=r'Receipts < USD $10^6$',
                   facecolors='cornflowerblue', edgecolors='blue', marker='s')
        #ax.scatter(names_IDS, s10Prop.values(), label=r'Receipts < USD $10^7$', facecolors='blue', edgecolors='midnightblue',marker='*')
        #plt.legend()
        plt.xlabel('Sector', fontsize=14)
        plt.ylabel(ylabel, fontsize=14)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.legend(fontsize=14)
        plt.grid()
        ax.set_xticks(names_IDS)
        ax.set_xticklabels(names)
        #plt.xticks(rotation="vertical")
        plt.xticks(rotation=50)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.1,
                         box.width, box.height * 0.9])

        modSaveName = '1' + saveName
        plt.savefig(modSaveName)
        plt.close()


        fig, ax = plt.subplots()
        fig.subplots_adjust(bottom=0.2)
        ax.scatter(names_IDS, s10Prop.values(), label=r'Receipts < USD $10^7$',facecolors='cornflowerblue', edgecolors='blue', marker='s')
        # fig.subplots_adjust(bottom=0)
        plt.xlabel('Sector', fontsize=14)
        plt.ylabel(ylabel, fontsize=14)
        plt.title(title, fontsize=18)
        plt.ylim(ymin=0)
        plt.legend(fontsize=14)
        plt.grid()
        ax.set_xticks(names_IDS)
        ax.set_xticklabels(names)
        plt.xticks(rotation=50)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.1,
                         box.width, box.height * 0.9])
        saveNameMod = '10' + saveName
        plt.savefig(saveNameMod)
        plt.close()


    outDirect()

    a = all.values()
    a = [int(i) for i in a]
    b = s1.values()
    b = [int(i) for i in b]
    c = s10.values()
    c = [int(i) for i in c]

def all():
    naicsCodes()
    conversionCodes()
    sizeNaics()
    minimalCodes()
    dataMinimalCodes()
    SIC()
    SICtwoDigit()
    majorIndustry()
    naicsMAJ()
    majNAICS()
    smallTotalNaics()

def makePlots():
    fileNames = ['revAllMinSIC2DMajor.txt',
                 'revS0MinSIC2DMajor.txt',
                 'revS1MinSIC2DMajor.txt',
                 'revS10MinSIC2DMajor.txt']
    title = 'Share of small firms by major sector'
    ylabel = 'Share of receipts'
    saveName = 'receiptsSIC.png'
    plots(fileNames, title, ylabel, saveName, False)

    fileNames = ['revAllMAJGroup.txt',
                 'revS0MAJGroup.txt',
                 'revS1MAJGroup.txt',
                 'revS10MAJGroup.txt']
    title = 'Share of small firms by major sector'
    ylabel = 'Share of receipts'
    saveName = 'receiptsNAICS.png'
    plots(fileNames, title, ylabel, saveName, False)


    fileNames = ['revAllMin.txt',
                 'revS0Min.txt',
                 'revS1Min.txt',
                 'revS10Min.txt']

    title = 'Distribution of small firms by sector'

    plots(fileNames, title, None, None, True)

all()
makePlots()